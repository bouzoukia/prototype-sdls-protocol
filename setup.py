from setuptools import setup
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='python-sdls',
    version='0.0.1',
    description='Python implementation of CCSDS Space Data Link Security Protocol',
    long_description=long_description,
    license='MIT',
    python_requires='>=3',
    keywords='ccsds sdls',
    packages=['sdls'],
    install_requires=[],
)

