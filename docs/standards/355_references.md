[D1] Information Technology—Open Systems Interconnection—Basic Reference Model:
    The Basic Model. 2nd ed. International Standard, ISO/IEC 7498-1:1994. Geneva:
    ISO, 1994.
[D2] Information Processing Systems—Open Systems Interconnection—Basic Reference
    Model—Part 2: Security Architecture. International Standard, ISO 7498-2:1989.
    Geneva: ISO, 1989.
[D3] Space Data Link Security Concept of Operation. Report Concerning Space Data
    System Standards, CCSDS 350.5-G. Washington, D.C.: CCSDS, forthcoming.
[D4] Overview of Space Communications Protocols. Issue 3. Report Concerning Space
    Data System Standards (Green Book), CCSDS 130.0-G-3. Washington, D.C.:
    CCSDS, July 2014.
[D5] Information Security Glossary of Terms. Issue 1. Report Concerning Space Data
    System Standards (Green Book), CCSDS 350.8-G-1. Washington, D.C.: CCSDS,
       November 2012.
[D6] TM Synchronization and Channel Coding. Issue 2. Recommendation for Space Data
       System Standards (Blue Book), CCSDS 131.0-B-2. Washington, D.C.: CCSDS,
       August 2011.
[D7] The Application of CCSDS Protocols to Secure Systems. Issue 2. Report Concerning
       Space Data System Standards (Green Book), CCSDS 350.0-G-2. Washington, D.C.:
       CCSDS, January 2006.
[D8] Security Architecture for Space Data Systems. Issue 1. Recommendation for Space
       Data System Practices (Magenta Book), CCSDS 351.0-M-1. Washington, D.C.:
       CCSDS, November 2012.
[D9] Space Missions Key Management Concept. Issue 1. Report Concerning Space Data
       System Standards (Green Book), CCSDS 350.6-G-1. Washington, D.C.: CCSDS,
       November 2011.
[D10] National Information Assurance (IA) Glossary. Revised. CNSSI No. 4009. Fort
       Meade, Maryland: CNSS, April 6, 2015.
[D11] Glossary of Key Information Security Terms. Rev. 2. Edited by Richard Kissel. NIST
       IR 7298. Gaithersburg, Maryland: NIST, May 2013.
[D12] Elaine Barker, et al. Recommendation for Key Management—Part 1: General. Rev. 3.
       National Institute of Standards and Technology Special Publication 800-57.
       Gaithersburg, Maryland: NIST, July 2012.
[D13] Information Technology—Open Systems Interconnection—Conformance Testing
       Methodology and Framework—Part 7: Implementation Conformance Statements.
       International Standard, ISO/IEC 9646-7:1995. Geneva: ISO, 1995.
[D14] Space Data Link Security Protocol—Extended Procedures. Recommendation for
       Space Data System Standards, CCSDS 355.1-B. Washington, D.C.: CCSDS,
       forthcoming.
