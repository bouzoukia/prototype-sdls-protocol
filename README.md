python-sle
==========

Implementation of the CCSDS SPace Data Link Security (SDLS) Protocol in Python3.

Getting Started
---------------

Make sure you are on Python3. Then follow the steps:

.. code:: bash

git clone https://gitlab.com/librecube/prototypes/python-sle.git
cd python-sle
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
pip install -e .



